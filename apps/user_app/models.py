from database import Base
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    is_active = Column(Boolean, default=True)
    type = Column(String, default="guest")
    items = relationship(
        "Item",
        back_populates="owner",
        cascade="all,delete-orphan",
        passive_deletes=True,
    )
