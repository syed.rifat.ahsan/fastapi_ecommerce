from pydantic import BaseModel
from typing import Optional, List


class User(BaseModel):
    name: str
    email: str
    password: str
    is_active: bool
    type: str


class UserView(BaseModel):
    id: int
    name: Optional[str] = None
    email: Optional[str] = None
    type: Optional[str] = None

    class Config:
        orm_mode = True


class UserViewList(BaseModel):
    status: bool
    user_list: List[UserView]

    class Config:
        orm_mode = True


class UserUpdate(BaseModel):
    # name: str | None = None
    name: Optional[str]
    email: Optional[str]
    password: Optional[str]
    is_active: Optional[str]
    type: Optional[str]
