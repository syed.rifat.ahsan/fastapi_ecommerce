from pydantic import BaseModel
from typing import Optional, List


class LoginData(BaseModel):
    grant_type: Optional[str] = None
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class TokenData(BaseModel):
    username: str | None = None
