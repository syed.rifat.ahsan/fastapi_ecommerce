from pydantic import BaseModel
from typing import Optional, List
import apps.user_app.schema as user_schema


class Item(BaseModel):
    title: str
    description: str
    owner_id: int

    class Config:
        orm_mode = True


class ItemView(BaseModel):
    title: str
    description: str
    owner: user_schema.UserView

    class Config:
        orm_mode = True


class ItemList(BaseModel):
    items: List[ItemView]

    class Config:
        orm_mode = True
