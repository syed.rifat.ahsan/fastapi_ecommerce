from fastapi import FastAPI
import database
import uvicorn
from apps.user_app import models as user_model
from apps.items_app import models as item_model
import apps.user_app.route as user_route
import apps.items_app.route as items_route

user_model.Base.metadata.create_all(database.engine)
item_model.Base.metadata.create_all(database.engine)

app = FastAPI()
app.include_router(user_route.router)
app.include_router(items_route.router)
# app.include_router(auth.router)
if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=9000)
